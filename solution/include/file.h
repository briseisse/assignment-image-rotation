//
// Created by brune on 12/03/2022.
//

#ifndef FILE_H
#define FILE_H

#include <stdio.h>

FILE* open_file(const char* file_name, const char* mode);
void close_file( FILE* file);

#endif
