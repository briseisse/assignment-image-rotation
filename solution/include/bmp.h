//
// Created by brune on 12/03/2022.
//

#ifndef BMP_H
#define BMP_H


#include "image.h"
#include <stdint.h>
#include <stdio.h>

enum bmp_read_write_status {
    READ_BMP_OK = 0,
    READ_BMP_INVALID_SIGNATURE,
    READ_BMP_INVALID_BITS,
    READ_BMP_INVALID_HEADER,
    READ_BMP_FAILED,
    WRITE_BMP_OK = 0,
    WRITE_BMP_ERROR,
};

enum bmp_read_write_status from_bmp(FILE* in, struct image* img);
enum bmp_read_write_status to_bmp(FILE* out, struct image* img);

#endif


