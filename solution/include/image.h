//
// Created by brune on 12/03/2022.
//

#include <stdint.h>
#include <stdio.h>
#pragma once

struct image{
    size_t width, height;
    struct pixel* data;
};

struct pixel {
    uint8_t b, g, r;
};

struct image rotate_by_90_degrees(struct image const* source);
