//
// Created by brune on 11/03/2022.
//
#include "../include/image.h"

#include <malloc.h>

static void update_image(struct image* out, struct image const* source, int i, int j){
    out->data[i * out->width + j] = (source->data[(source->height - 1 - j) * source->width + i]);
}

struct image rotate_by_90_degrees(struct image const* source){
    struct image out = {0};
    out.width = source->height;
    out.height = source->width;
    out.data = malloc(sizeof(struct pixel) * source->width * source->height);
    for(size_t i =0; i <source->width; i++){
        for(size_t j = 0; j < source->height; j++){
            update_image(&out, source, i ,j);
        }
    }
    return out;
}
