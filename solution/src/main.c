#include "../include/bmp.h"
#include "../include/bmp_header.h"
#include "../include/file.h"
#include "../include/image.h"
#include <stdio.h>


int main( int argc, char** argv ) {
    (void) argc; (void) argv;
    if(argc != 3){
        printf("Please, check your arguments! \n ");
        return 1;
    }

    FILE* input_image = open_file(argv[1],"r");
    FILE* output_image = open_file(argv[2],"w");
    struct image image = {0};
    if(from_bmp(input_image, &image) != READ_BMP_OK){
        printf("File cannot be read! \n");
        close_file(input_image);
        close_file(output_image);
        return 1;
    }
    close_file(input_image);
    struct image rotated_image = rotate_by_90_degrees(&image);
    if(to_bmp(output_image,&rotated_image) != WRITE_BMP_OK){
        printf("Cannot write in this file!!");
        close_file(output_image);
        return 1;
    }
    close_file(output_image);
    free(image.data);
    free(rotated_image.data);
    return 0;
}
