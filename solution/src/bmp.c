//
// Created by brune on 11/03/2022.
//
#define DWORD (4)
#define BMP_HEADER_SIZE (40)
#define BMP_RESERVED 0
#define BMP_TYPE (0x4D42)
#define BMP_PLANES (1)
#define BMP_BITCOUNT (24)


#include "../include/bmp_header.h"
#include "../include/bmp.h"
#include "../include/image.h"
#include <malloc.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>



static enum bmp_read_write_status read_bmp_header(FILE* in, struct bmp_header* header){
    if (fseek(in, 0, SEEK_END) != 0){
        return READ_BMP_FAILED;
    }
    size_t size = ftell(in);
    if(size < sizeof(struct bmp_header)){
        return READ_BMP_INVALID_HEADER;
    }
    rewind(in);
    if(fread(header, sizeof(struct bmp_header), 1, in) != 1){
        return READ_BMP_FAILED;
    }
    return READ_BMP_OK;
}

enum bmp_read_write_status read_data(struct image* img, FILE* in, uint8_t padding){
    size_t width = img->width;
    size_t height = img->height;
    struct pixel* data = malloc (width * height * sizeof(struct pixel));
    for( size_t i = 0; i < height; i ++){
        fread(data+i*width, sizeof(struct pixel), width, in);
        fseek(in, padding, SEEK_CUR);
    }
    img->data = data;
    return READ_BMP_OK;
}



static uint32_t get_padding(uint32_t width){
    if(width %4 == 0){
        return 0;
    }
    else{
        return DWORD - (width*(BMP_BITCOUNT / 8)) % DWORD;
    }
}

enum bmp_read_write_status from_bmp(FILE* in, struct image* img){
    struct bmp_header header = {0};
    enum bmp_read_write_status status = read_bmp_header(in, &header);
    if (status != READ_BMP_OK) return status;
    const uint32_t width = header.biWidth;
    const uint32_t height = header.biHeight;
    const uint32_t padding = get_padding(width);
    img->width = width;
    img->height = height;
    return read_data(img, in, padding);
}

enum bmp_read_write_status create_header(FILE* const out, const size_t width, const size_t height){
    uint8_t padding = get_padding(width);
    const size_t image_size = (sizeof(struct pixel) * (width) + padding)* height;
    const struct bmp_header header = {
            .bfType = BMP_TYPE,
            .bfileSize = sizeof(struct bmp_header) + image_size,
            .bfReserved = BMP_RESERVED,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = BMP_HEADER_SIZE,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = BMP_PLANES,
            .biBitCount = BMP_BITCOUNT,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biCompression = 0,
            .biSizeImage = image_size,
            .biClrUsed = 0,
            .biClrImportant = 0,
    };

    if ( fwrite(&header, sizeof(struct bmp_header), 1, out) != 1){
        return WRITE_BMP_ERROR;
    }
    return WRITE_BMP_OK;
}

enum bmp_read_write_status to_bmp(FILE* out, struct image* img){
    if (!out || !img) return WRITE_BMP_ERROR;
    const uint32_t width = img->width;
    const uint32_t height = img->height;
    const enum bmp_read_write_status status = create_header(out, width, height);
    if (status != WRITE_BMP_OK) return status;
    uint8_t padding = get_padding(width);
    uint8_t* const paddings[3] = {0};
    for (size_t i = 0; i < height; i++){
        if (!fwrite((img->data) + i * width, sizeof(struct pixel)* width, 1, out)) return WRITE_BMP_ERROR;
        if (!fwrite(paddings, padding, 1, out)) return WRITE_BMP_ERROR;
    }
    return WRITE_BMP_OK;
}
