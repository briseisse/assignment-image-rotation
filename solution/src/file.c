//
// Created by brune on 11/03/2022.
//

#include "../include/file.h"
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>




FILE* open_file(const char* file_name, const char* mode) {
    
    return fopen(file_name, mode);
}

void close_file(FILE* file){
    if (file == NULL){
       printf("FILE_DOES_NOT_EXIST"); 
        }
    else{
        printf("FILE_CLOSED_OK");
        }
}





